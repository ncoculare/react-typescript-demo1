import {
  getItemFromLocalStorage,
  removeItemLocalStorage,
  setItemLocalStorage
} from './localstorage.helper';

export interface Credentials {
  username: string;
  password: string;
}

export async function signIn(
  url: string,
  params: Credentials
) {
  const api = `${url}?username=${params.username}&password=${params.password}`;

  const response = await fetch(api, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  const result = await response.json();
  setItemLocalStorage('token', result.accessToken);
  return result;
}


export function signOut() {
  removeItemLocalStorage('token');
}

export function isLogged(): boolean {
  return !!getItemFromLocalStorage('token')
}

