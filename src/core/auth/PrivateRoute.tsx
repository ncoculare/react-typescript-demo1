import React from 'react';
import { Redirect, Route } from 'react-router';
import { isLogged } from './authentication.service';

interface PrivateRouteProps {
  path: string;
  children: React.ReactNode;
}

export const PrivateRoute: React.FC<PrivateRouteProps> = ({ children, ...rest }) => {
  return (
    <Route {...rest}>
      {
        isLogged() ?
          children :
          <Redirect to={{ pathname: "/login" }} />
      }
    </Route>
  );
};
