import React from 'react';
import { useHistory } from 'react-router';
import { Credentials, signIn } from '../core/auth/authentication.service';

export const PageLogin: React.FC = () => {
  let history = useHistory();

  let login = () => {
    const credentials: Credentials = {
      username: 'abc',
      password: '123'
    };

    signIn('http://localhost:3001/login', credentials)
      .then(() => {
        history.replace('home');
      })
  };

  return (
    <div>
      <p>You must log in to view the private pages (Settings & Admin)</p>
      <button onClick={login}>Log in</button>
    </div>
  );
};
